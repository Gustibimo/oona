# Skill Test for OONA

## Implementation Guidelines
- You can choose two out of three tasks, but we will be very happy if you can complete all the tasks given.
- Please finish the tasks in 24 hours.
- After you finish, push your code into git repo (github/bitbucket/gitlab).
- Use Go or Python.
- You will need to determine how to pass the input data to your application (reading from a file is ok).
- Provide instructions on how to run and test your application.
- Document any assumptions and important design decisions.
- If possible provide also unit test for your codes.
- Use the most optimal codes that you can, think of having those example in production.


