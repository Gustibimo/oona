import operator
import sys
word2num = {'satu': 1, 'dua':2, 'tiga': 3, 'empat': 4, \
            'lima': 5, 'enam': 6, 'tujuh': 7, 'delapan': 8, \
            'sembilan': 9, 'sepuluh':10, 'sebelas':11, 'dua belas': 12, 'tiga belas':13}

n2w = {1: 'satu', 2: 'dua', 3: 'tiga', 4: 'empat', 5: 'lima', 6: 'enam', 7: 'tujuh', 8: 'delapan', 9: 'sembilan', 10: 'sepuluh', 11: 'sebelas', 12: 'dua belas', 13: 'tiga belas'}



def calc(word):
    temp = word.split(" ")
    angka1 = temp[0]
    operator1 = temp[1]
    angka2 = temp[2]

    ops = { "tambah": operator.add, "kurang": operator.sub, "kali": operator.mul, "bagi": operator.floordiv } # etc.
    result = ops[operator1](word2num[angka1],word2num[angka2])
    print(word + " adalah " + n2w[result])


calc("tiga kali dua")
calc("enam bagi dua")
