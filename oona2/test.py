import unittest

from oona2 import maxSubArraySum

a = [3,-9,-1,4,3,-2,3]

class OonaTest(unittest.TestCase):
    def test(self):
        self.assertEqual(maxSubArraySum(a,len(a)), 8)

if __name__ == '__main__':
    unittest.main()