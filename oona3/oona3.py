
# from data import listData
def stat(listData):
    frigeData = {}
    result = {}
    for data in listData:
        if data["id"] in frigeData:
            frigeData[data["id"]].append(data["temperature"])
        else:
            frigeData[data["id"]] = [data["temperature"]]
    
    listResult = []
    for k,v in frigeData.items():
        res ={}
        result[k] = mean(v)
        res["id"] = k
        res["average"] = mean(v)
        res["median"] = median(v)
        res["mode"] = mode(v)
        listResult.append(res)
    return listResult

def mean(numbers):
    return round(sum(numbers) / float(len(numbers)),2)

def median(lst):
    n = len(lst)
    if n < 1:
            return None
    if n % 2 == 1:
            return sorted(lst)[n//2]
    else:
            return round(sum(sorted(lst)[n//2-1:n//2+1])/2.0,2)
def mode(lst):
    modeCount = [x for x in set(lst) if lst.count(x) > 1]
    return modeCount

        
# print(stat(listData))