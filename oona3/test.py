import unittest

from oona3 import stat

listData = [
{"id": "a","timestamp": 1509493641,"temperature": 3.53},
{"id": "b","timestamp": 1509493642,"temperature": 4.13},
{"id": "c","timestamp": 1509493643,"temperature": 3.96},
{"id": "a","timestamp": 1509493644,"temperature": 3.63},
{"id": "c","timestamp": 1509493645,"temperature": 3.96},
{"id": "a","timestamp": 1509493645,"temperature": 4.63},
{"id": "a","timestamp": 1509493646,"temperature": 3.53},
{"id": "b","timestamp": 1509493647,"temperature": 4.15}
]

# print(stat(listData))
expected =[{'id': 'a', 'average': 3.83, 'median': 3.58, 'mode': [3.53]}, {'id': 'b', 'average': 4.14, 'median': 4.14, 'mode': []}, {'id': 'c', 'average': 3.96, 'median': 3.96, 'mode': [3.96]}]

class MyTest(unittest.TestCase):
    def test(self):
        self.assertEqual(stat(listData), expected)

if __name__ == '__main__':
    unittest.main()